const protoBuf = require(`protobufjs`);
const servicePath = `node_modules/bambu-proto/master.proto`;
const config = require(`../../config/config`);
const bambuService = require(`../../service/grpc/bambuService`);

protoBuf.load(servicePath, function (err, root) {
    if (err) throw err;

    // create server
    const grpc = require(`grpc`);
    const server = new grpc.Server();
    const proto = grpc.load(servicePath);

    server.addService(proto.bambuService.BambuService.service, {
        getPeopleLikeYou : bambuService.getPeopleLikeYou,
    });

    //Specify the IP and and port to start the grpc Server, no SSL in test environment
    const grpcURL = `0.0.0.0:2211`;
    server.bind(grpcURL, grpc.ServerCredentials.createInsecure());

    //Start the server
    server.start();
    console.log(`grpc server running on [${grpcURL}]`);
});

process.on(`uncaughtException`, function (err) {
    console.log({reason: `uncaughtException`, message: err.message.toString(), stack: err.stack});
});
