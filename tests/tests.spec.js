const assert = require(`assert`);
const esClient = require(`../model/elasticsearch/elasticsearch`);
const people = require(`../usecase/people`);


describe('Test :: ES MODEL', function () {
    it('should return results from ES', function () {
        const index = `bambu`;
        const docType = `_doc`;
        const query = {"query":{"bool":{"must":[{"match_all":{}}]}},"size":30};

        esClient.search(index, docType, query, (err, resp) => {
            const hits = resp.hits.hits;
            assert.equal(hits.length, 30);
        });
    });
});


describe('Test :: Usecase : Find similar investors', function () {
    it('should return similar people', function () {
        const payload = {
                            "name":"raj",
                            "age":"25",
                            "latitude":"50.5345697",
                            "longitude":"19.56667",
                            "monthlyIncome":"9500",
                            "experienced":"false"
                        };
        people.getPeopleLikeYou(payload, (err, resp) => {
            assert.equal(resp.length, 10);
        });
    });
});
