const elasticsearch = require('elasticsearch');

const client = new elasticsearch.Client( {  
    host: '128.199.85.12:9200',
    log: 'error'
});

module.exports = client;
