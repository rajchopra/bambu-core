const client = require('./connection');

module.exports = {
	// 1. Ping to check the connection
	ping: function(callback){
		client.ping({
		  	requestTimeout: 30000,
		}, function (error) {
			if (error) {
			    return callback({status: false, msg: 'Elasticsearch cluster is down!'});
			} else {
			    return callback({status: true, msg: 'Success! Elasticsearch cluster is up!'});
			}
		});
	},

	// 2. Add/Update a document
	addDocument: function(index, _id, docType, payload, callback){
	    client.index({
	        index: index,
	        type: docType,
	        id: _id,
	        body: payload
	    }).then(function (resp) {
	        // console.log(resp);
	        return callback();
	    }, function (err) {
	        // console.log(err.message);
	        return callback(err);
	    });
	},

	// 3. Search
	search: function(index, docType, payload, callback){
		client.search({
	        index: index,
	        type: docType,
	        body: payload
	    }).then(function (resp) {
	        // console.log(resp);
	        return callback(null, resp);
	    }, function (err) {
	        // console.log(err.message);
	        return callback(err.message);
	    });
	},
};