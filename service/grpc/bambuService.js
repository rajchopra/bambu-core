const config = require('../../config/config');
const people = require('../../usecase/people');

const service = {};

// Service method to get similar people
service.getPeopleLikeYou = function (call, cb) {
    console.log(`SERVICE:BAMBU_SERVICE:getPeopleLikeYou. Request[${JSON.stringify(call.request)}]`);
    const payload = call.request;
    return people.getPeopleLikeYou(payload, cb);
};

module.exports = service;