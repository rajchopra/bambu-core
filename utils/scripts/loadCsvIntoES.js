const fs = require(`fs`);
const parse = require(`csv-parse`);
const async = require(`async`);
const uuid = require(`uuid`);

const client = require(`../../model/elasticsearch/elasticsearch`);
const inputFile = `../../../backend-challange-v2/data/data.csv`;

let counter = 1;

const parser = parse({delimiter: `,`}, function (err, data) {
  async.eachLimit(data, 200, function (line, callback) {
    // console.log(line);

    // First line is headings, ignore it.
    if (counter == 1) {
        counter++;
        callback();
    } else {
        const name = line[0];
        const age = line[1];
        const lat = line[2];
        const long = line[3];
        const income = line[4];
        const experienced = line[5];

        const payload = {
            "name": name,
            "age": age,
            "income": income,
            "experienced": experienced,
            "pin" : {
                "location" : { 
                    "lat": lat,
                    "lon": long
                }
            }
        };

        const index = `bambu`;
        const docType = `_doc`;

        _id = uuid.v4();

        client.addDocument(index, _id, docType, payload, (err) => {
            if(err) console.log(`[${counter++}] Failed to add to ES. Error[${err}]`);
            else console.log(`[${counter++}] Successfully added to ES.`);
            callback();
        });
    }
  });
});
fs.createReadStream(inputFile).pipe(parser);

process.on(`exit`, function (code) {
    console.log(`Exit code [${code}]`);
});

process.on(`SIGINT`, function () {
    // client.close(); // close the elasticsearch connection
    process.kill(process.pid, `SIGKILL`);
});

process.on(`uncaughtException`, function (err) {
    console.log(err);
    console.log(err.message);
});