Task has been hosted on domain
[http://bambutask.ml](https://www.google.com/url?q=http://bambutask.ml/people-like-you&sa=D&ust=1548024387609000)

Sample Requests :

-   Request without any query parameters :

-   [http://bambutask.ml/people-like-you](https://www.google.com/url?q=http://bambutask.ml/people-like-you&sa=D&ust=1548024387609000)

-   Request with all the query parameters :

-   [http://bambutask.ml/people-like-you?name=raj&age=23&latitude=50.5345697&longitude=19.56667&monthlyIncome=4100&experienced=true](https://www.google.com/url?q=http://bambutask.ml/people-like-you?name%3Draj%26age%3D23%26latitude%3D50.5345697%26longitude%3D19.56667%26monthlyIncome%3D4100%26experienced%3Dtrue&sa=D&ust=1548024387610000)

-   Request with just geo-coordinates as query parameters :

-   [http://bambutask.ml/people-like-you?latitude=50.5345697&longitude=19.56667](https://www.google.com/url?q=http://bambutask.ml/people-like-you?latitude%3D50.5345697%26longitude%3D19.56667&sa=D&ust=1548024387610000)

-   Request with impracticable age:

-   [http://bambutask.ml/people-like-you?age=1000](https://www.google.com/url?q=http://bambutask.ml/people-like-you?age%3D1000&sa=D&ust=1548024387610000)

-   Request with just age as query parameter :

-   [http://bambutask.ml/people-like-you?age=25](https://www.google.com/url?q=http://bambutask.ml/people-like-you?age%3D25&sa=D&ust=1548024387611000)

-   Request with just experience as query parameter :

-   [http://bambutask.ml/people-like-you?experienced=true](https://www.google.com/url?q=http://bambutask.ml/people-like-you?experienced%3Dtrue&sa=D&ust=1548024387611000)

Architecture Diagram

![Architecture Diagram](https://i.ibb.co/pdc7wh4/image1.png)

Microservices architecture containing :

1.  Bambu-core service
2.  Bambu-API service
3.  ElasticSearch DB

Microservices interaction :

-   Inter-service communication between Bambu-API service and Bambu-core
    service happens via GRPC.
-   Bambu-API service exposes a HTTP endpoint to public internet.

Repository details :

1.  Bambu-core

url :
 [https://bitbucket.org/rajchopra/bambu-core](https://www.google.com/url?q=https://bitbucket.org/rajchopra/bambu-core&sa=D&ust=1548024387613000)

This repo contains the main business logic to find similar investors
based on query parameters.

This connects to ElasticSearch to fetch the data.

2.  Bambu-API

url :
[https://bitbucket.org/rajchopra/bambu-api](https://www.google.com/url?q=https://bitbucket.org/rajchopra/bambu-api&sa=D&ust=1548024387614000)

This repo contains the API that is exposed to the public internet.

It’s running a webserver and has exposed an endpoint /people-like-you

This is dependent on bambu-proto repo, to connect to bambu-core service
using GRPC.

3.  Bambu-proto

url :
[https://bitbucket.org/rajchopra/bambu-proto](https://www.google.com/url?q=https://bitbucket.org/rajchopra/bambu-proto&sa=D&ust=1548024387615000)

This repo has the proto schema used while communication between
bambu-api and bambu-core.

This also contains the nodejs client, which is directly used by
bambu-api to connect to bambu-core service.

To setup Bambu-API :

-   git clone https://raj\_chopra@bitbucket.org/rajchopra/bambu-api.git
-   cd bambu-api/app ;
-   To install packages : npm install
-   To start service : npm start
-   To test : npm test

To setup Bambu-core :

-   git clone https://raj\_chopra@bitbucket.org/rajchopra/bambu-core.git
-   cd bambu-core ;
-   To install packages : npm install
-   To start service : npm start
-   To test : npm test

To setup ElasticSearch:

-   Install ElasticSearch on the server
-   Update ES credentials and populate data : node
    utils/scripts/loadCsvIntoES.js



