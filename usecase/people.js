const esClient = require(`../model/elasticsearch/elasticsearch`);
const async    = require(`async`);
const usecase  = {};

// usecase method to get similar people
usecase.getPeopleLikeYou = function (payload, cb) {

    // console.log(`Request params=[${JSON.stringify(payload)}]`);
    const index = `bambu`;
    const docType = `_doc`;
    const resultCount = 30; // Getting 30 results from ES. Will find 10 best results to return as response.

    const correctPayload = {};
    correctPayload.name = payload.name;

    if (!payload.age) {
        correctPayload.age = parseInt(payload.age, 10);
    }

    if (!payload.latitude) {
        correctPayload.latitude = parseFloat(payload.latitude);
    }

    if (!payload.longitude) {
        correctPayload.longitude = parseFloat(payload.longitude);
    }

    if (!payload.monthlyIncome) {
        correctPayload.monthlyIncome = parseInt(payload.monthlyIncome, 10);
    }

    correctPayload.experienced = payload.experienced;
    
    async.waterfall([
        function (callback) {
            createQuery(correctPayload, resultCount, callback);
        },
        function (query, callback) {
            getResults(index, docType, query, callback);
        },
        function (resultsArr, callback) {
            assignScore(resultsArr, correctPayload, callback);
        },
        function (resultsArrWithScore, callback) {
            getBestResults(resultsArrWithScore, callback);
        }
    ], function (err, finalResultsArr) {
            if (err) {
                console.log(err);
                cb(err);
            } else {
                cb(null, finalResultsArr);
            }
    });
};


// internal method 

// Method to get results from ES
const getResults = function(index, docType, query, callback) {
    
    esClient.search(index, docType, query, (err, resp) => {
        if (err){
            console.log(`ERROR : ${err}`);
            callback(err);
        } else {
            // console.log(`Got response from ES [${resp}]`);
            const hits = resp.hits.hits;
            const counter = 1;
            const resultsArr = [];
            for (const hit of hits) {
                const data = hit._source;
                if (hit.sort) data.sort = hit.sort[0];
                resultsArr.push(data);
            }
            // console.log(`resultsArr :: ${JSON.stringify(resultsArr)}`);

            callback(null, resultsArr);
        }
    });
};



// Method to assign score to results  
const assignScore = function (resultsArr, payload, callback) {

    const finalResult = {};

    for (const result of resultsArr) {
        let score = 1.0; // default score
        const thisResult = {};
        // console.log(JSON.stringify(result));
        thisResult.name = result.name;
        thisResult.age = parseInt(result.age, 10);
        thisResult.latitude = parseFloat(result.pin.location.lat);
        thisResult.longitude = parseFloat(result.pin.location.lon);
        thisResult.monthlyIncome = parseInt(result.income, 10);
        thisResult.experienced = (result.experienced == `true`);
        
        if (payload.age && (payload.age != thisResult.age)) {
            score = 1.0 - (Math.abs(payload.age - thisResult.age))/10;
        }
        
        // console.log(`Actual Score[${score}]; Distance[${result.sort}];`);
        if (payload.latitude && payload.longitude) {
            if (result.sort < 200) {
                // best case (Result's location is within 500kms of queried geocoordinates);
                // score not modified
            } else if (result.sort < 500) {
                score -= 0.1;                
            } else if (result.sort < 2000) {
                score -= 0.2;
            } else if (result.sort < 5000) {
                score -= 0.4;
            } else if (result.sort < 10000) {
                score -= 0.6;
            } else if (result.sort < 22000) {
                score -= 0.8;
            } else if (result.sort >= 22000) {
                score -= 0.9;
            }
        }
        // console.log(`Updated Score[${score}];`);

        // score value is between 0 and 1, it can not be negative, handle that case.
        if (score < 0) {
            score = 0;
        }

        if (!finalResult[score]) finalResult[score] = [];
        finalResult[score].push(thisResult);
    }
    callback(null, finalResult);
};


// Method to extract top results from available results  
const getBestResults = function (resultsArr, callback) {

    const finalResult = [];
    const scoreArr = [1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0];

    let count = 0;

    for (const currScore of scoreArr) {
        if (count == 10) break;
        const currResultsArr = resultsArr[currScore];

        if (currResultsArr) {
            for (const result of currResultsArr) {
                result.score = currScore;
                finalResult.push(result);
                count++;
                if (count == 10) break;
            }
        }
    }
    callback(null, finalResult);
};


// Method to create query object
const createQuery = function (payload, resultCount, callback) {

    // console.log(`payload=[${JSON.stringify(payload)}]`);
    const age = payload.age;
    const latitude = payload.latitude;
    const longitude = payload.longitude;
    const monthlyIncome = payload.monthlyIncome;
    const experienced = payload.experienced;

    let query = {};
    let sortQuery = null;
    let mustArr = [];


    // Append age query if required.
    if (payload.age) {
        const minAge = age - 1;
        const maxAge = age + 1;
        
        const ageQuery =  {
            "range" : {
                "age" : {
                    "gte" : minAge,
                    "lte" : maxAge,
                    "boost" : 3.0
                }
            }
        };
        mustArr.push(ageQuery);
    }

    // Append income query if required.
    if (payload.monthlyIncome) {
        const minIncome = monthlyIncome - 1000;
        const maxIncome = monthlyIncome + 1000;
        
        const incomeQuery =  {
            "range" : {
                "income" : {
                    "gte" : minIncome,
                    "lte" : maxIncome,
                    "boost" : 2.0
                }
            }
        };
        mustArr.push(incomeQuery);
    }


    // Append experienced query if required.
    if (experienced == `true` || experienced == `false`) {
        const expQuery =  {
            "match": {
                "experienced": experienced
            }
        };
        mustArr.push(expQuery);
    }

    if (mustArr.length == 0) {
        const matchAllQuery = { "match_all" : {}};
        mustArr.push(matchAllQuery);
    }

    query.bool = {};
    query.bool.must = mustArr;

    if (payload.latitude && payload.longitude) {
        query.bool.filter = {
                                "geo_distance" : {
                                    "distance" : "100000km",
                                    "pin.location" : {
                                        "lat" : latitude,
                                        "lon" : longitude
                                    }
                                }
                            };

        sortQuery = [
                            {
                                "_geo_distance": {
                                "pin.location": { 
                                    "lat":  latitude,
                                    "lon": longitude
                                },
                                "order":         "asc",
                                "unit":          "km", 
                                "distance_type": "plane" 
                            }
                        }
                    ];
    }


    // FailCheck : If query.bool is empty, callback with error.
    if (!query.bool) {
        const newErr = new Error(`Query could not be created for payload [${payload}]`);
        callback(newErr);
    } else {
        const finalQuery = {
            "query" : query,
            "size"  : resultCount
        };
        if (sortQuery) {
            finalQuery.sort = sortQuery;
        }
        // console.log(JSON.stringify(finalQuery));
        callback(null, finalQuery);
    }
};


module.exports = usecase;